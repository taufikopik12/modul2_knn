var training = [];
var k = 1;
var data;
var skip = 20;
var kSlider;
var kParagraph;
function setup() {
 createCanvas(800, 600);
 kSlider = createSlider(1, 10, 1);
 kParagraph = createP();
 kSlider.input(update);
 for (var i = 0; i < 50; i++) {
 var x = random(width);
 var y = random(height);
 var label = 'A';
 var r = random(1);
 if (r < 0.33) {
 label = 'B';
 } else if (r < 0.67) {
 label = 'C';
 }
 var point = {
 x: x,
 y: y,
 label: label
 };
 training.push(point);
 }
 update();
}
function update() {
 background(0);
 var k = kSlider.value();
 kParagraph.html('k: ' + k);
 for (var x = 0; x < width; x += skip) {
 for (var y = 0; y < height; y += skip) {
 var neighbors = [];
 for (var i = 0; i < training.length; i++) {
 var point = training[i];
 var d = dist(x, y, point.x, point.y);
 neighbors.push({
 dist: d,
 label: point.label
 });
 }
 neighbors.sort(byDistance);
 function byDistance(a, b) {
 return a.dist - b.dist;
 }
 var knn = {};
 for (var i = 0; i < k; i++) {
 var nb = neighbors[i];
 if (knn[nb.label]) {
 knn[nb.label]++;
 } else {
 // Otherwise start with 1
 knn[nb.label] = 1;
 }
 }
 var options = Object.keys(knn);

 var record = 0;
 var classification = null;
 for (var i = 0; i < options.length; i++) {
 var label = options[i];
 var total = knn[label];

 if (total > record) {
 record = total;

 classification = label;
 }
 }
 if (classification == 'A') {
 fill(255, 255, 0, 200);
 } else if (classification == 'B') {
 fill(0, 255, 255, 200);
 } else {
 fill(255, 0, 255, 200);
 }
 noStroke();
 rect(x, y, skip, skip);
 }
 }
 for (var i = 0; i < training.length; i++) {
 var point = training[i];
 if (point.label == 'A') {
 fill(255, 255, 0, 200);
 } else if (point.label == 'B') {
 fill(0, 255, 255, 200);
 } else {
 fill(255, 0, 255, 200);
 }
 stroke(0);
 ellipse(point.x, point.y, 24, 24);
 noStroke();
 fill(0);
 textAlign(CENTER);
 textSize(12);
 text(point.label, point.x, point.y + 6);
 }
}